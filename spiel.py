import gettext

_ = gettext.gettext


def main():
    print(_('Please translate me! Thanks'))

if __name__ == '__main__':
    main()

    sys.exit()
