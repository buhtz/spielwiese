---
gitea: none



include_toc: true
---

# spielwiese

Line one
Line two
Line three
Line four

# Rule 1
Description for rule 1.

Hotfix line.

Noch eine.

<div style="-webkit-column-count: 2; -moz-column-count: 2; column-count: 2; -webkit-column-rule: 1px dotted #e0e0e0; -moz-column-rule: 1px dotted #e0e0e0; column-rule: 1px dotted #e0e0e0;">
    <div style="display: inline-block;">
        <h2>Good</h2>
        <pre><code class="language-c">int foo (void) 
{
    int i;
}
</code></pre>
    </div>
    <div style="display: inline-block;">
        <h2>Bad</h2>
        <pre><code class="Python">def foobar(col):
        pass
</code></pre>
    </div>
</div>

# variante
<table>
<tr>
<th> Good </th>
<th> Bad </th>
</tr>
<tr>
<td>

```lisp
int foo() {
    int result = 4;
    return result;
}
```

</td>
<td>

```Python
def foo(col: int) -> str:
    x = 4
    return str(col)
```

</td>
</tr>
</table>

# Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
und weiter
