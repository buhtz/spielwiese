# Datensicherung mit Back In Time

[Back In Time](https://github.com/bit-team/backintime) (BIT) erstellt Datensicherungen und richtet sich an Endanwender:innen und Desktop-Nutzer:innen.

Um Speicherplatz einzusparen, macht die Anwendung im Hintergrund Gebrauch von Rsync's Hardlink Funktion. Vereinfacht formuliert, werden nur veränderte Dateien in den Backup-Ordner transferiert. Unveränderte Dateien werden nur als Hardlinks im Backup-Ordner angelegt.

Das Backup kann, unabhängig von BIT, mit jedem Dateimanager durchsucht und genutzt werden. Es kommen keine Container- oder Archiv-Formate zum Einsatz. 

Weitere Features:

* SSH, um Backups auf entfernte Server abzulegen.
* Verschlüsselung von Backups. Aktuell mit EncFS (deprecated) und langfristig mit gocryptfs.
* Umfangreiche Zeitsteuerung und Eventsteuerung (Anstecken an USB).
* User-callback Skript, um in einzelne Schritte des Backup-Prozesses einzugreifen.
* Regelbasiertes Entfernen älterer Backups.
* Ausführung als "root" (via "pkexec").

# Hinter dem Türchen

Neben der Installation und Einrichtung, stelle ich in diesem Beitrag  zwei Anwendungsfälle vor:
* Lokale Sicherung ("Lokal")
* Remote Sicherung ("SSH")
* Wiederherstellen einer Sicherung

Und am Ende erzähle ich noch kurz etwas zum aktuellen Status des Projektes und wie ich da hinein geraten bin. ;)

# Installation

Es gibt die zwei Pakete [deb]backintime-common[/deb] (Kommandozeilen-Tool) und [deb]backintime-qt[/deb] (die grafische Oberfläche).

    sudo apt install backintime-qt

Danach finden sich im Anwendungsmenü oder -starter die zwei Einträge "Back In Time" und "Back In Time (root)". Wir konzentrieren uns auf ersteres.

Beim ersten Start wird BIT fragen, ob eine frühere Konfiguration von einem anderen Ort importiert werden soll, was wir verneinen.

[[https://translate.codeberg.org/media/screenshots/bit_question_no_config.png]]

Danach sehen wir nicht das Hauptfenster, sondern gleich den Einstellungsdialog, um ein neues Backup-Profil einzurichten. Nicht verwirren lassen, BIT verwendet in älteren Versionen noch die Begriffe Snapshot oder Schnapschuss synonym für Backup.

# Lokale Sicherung

Der Einstellungs-Dialog (aka "Profile verwalten") ist über die Menü- oder Task-Leiste erreichbar und dürfte je nach Version in etwa so aussehen.

[[bit_mngprofiles_local.png]]

Hier in dem Screenshot sieht man den **Modus** "Lokal", welcher die einfachste Form darstellt. Dabei werden innerhalb des Verzeichnisbaums Dateien von einem Verzeichnis ins andere geschoben. Darunter (**Wo Schnappschüsse gespeichert werden**) wird das Ziel-Verzeichnis für die Backups angezeigt. Im darunterliegenden Abschnitt **Erweitert** wird ersichtlich, dass dieser Verzeichnis-Pfad um weitere Unterordner verlängert wird.

Im Tab **Einbeziehen** und **Ausschließen** lassen sich entsprechend Dateien und Verzeichnisse für die Sicherung auswählen und ggf. ausschließen.

Den Dialog schließen und dann im Hauptfenster das Backup anstoßen. Hier in Aktion auf einem etwas langsamen Raspberry Pi4:

[[bit_run_local.gif]]

# Remote Sicherung

Der Modus **SSH** ermöglicht es, eine Sicherung auf einem "entfernten" Verzeichnis via SSH anzulegen. Hier im Beispiel gehe ich davon aus, dass bereits eine passwordlose Anmeldung auf dem entfernten SSH Server  eingerichtet wurde. _Back In Time_ kann ggf. aber auch dabei unterstützen.

[[bit_mngprofiles_ssh.png]]

# Wiederherstellen einer Sicherung

TODO
[[bit_restore1.png]]
[[bit_restore2.png]]

```sh
$ cd ~/backme_up
$ ls -l
insgesamt 20K
drwxr-xr-x 2 user user 4,0K  7. Nov 11:15 apple/
drwxr-xr-x 2 user user 4,0K  6. Nov 16:48 banana/
drwxr-xr-x 2 user user 4,0K  7. Nov 11:15 choco/
drwxr-xr-x 2 user user 4,0K  7. Nov 11:15 date/
drwxr-xr-x 2 user user 4,0K  6. Nov 16:53 date_renamed/

$ ls -l **/*backup*
-rw-r--r-- 1 user user 29  6. Nov 16:54 apple/foobar.backup.20241107
```

# Mein _Back In Time_

SSH Profile
REstore Datei-Weise wie versionierung

# Zum Projekt und seinem aktuellen Status

_Back In Time_ ist etwa 15 Jahre alt und wird derzeit von der dritten Generation Maintainer betreut. Zusammen mit zwei weiteren Personen (Michael und Jürgen), bin ich seit Sommer 2022 Teil des Team. Zum  vormaligen Betreuer (Germar Reitze) haben wir noch sporadisch Kontakt. Der ursprüngliche Entwickler (Oprea Dan) ist nicht auffindbar. Die Arbeit hat manchmal etwas von IT-Archäologie.
Neben dem Lösen von Problemen und dem Verbessern der Funktionalität besteht ein beträchtlicher Teil der Betreuerarbeit derzeit darin, das Projekt und seine Infrastruktur auf moderne und effizienter wartbare Grundlagen zu stellen. Die Testsuite und die Dokumentation benötigen noch viel Arbeit und sind ein Grund dafür, warum einige scheinbar simple Probleme immer noch nicht behoben sind. Dazu mehr im [Strategy Outline](https://github.com/bit-team/backintime/blob/dev/CONTRIBUTING.md#strategy-outline) des Projekts.

Vielen Dank für die Aufmerksamkeit.

Christian
